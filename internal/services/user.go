package services

import (
	"context"
	"errors"
	"messaging_service/internal/models"
	"messaging_service/internal/repository"
	"messaging_service/internal/token"
	"time"

	"go.uber.org/zap"
)

type userService struct {
	repo           repository.User
	activityLogger Activity
	jwtHandler     token.JWTHandler
}

//go:generate mockery --name User --output ../mocks/mockuserservice --outpkg mockuserservice --case underscore --with-expecter
type User interface {
	CreateNewUser(ctx context.Context, creds models.NewUser) (string, error)
	AuthenticateUser(ctx context.Context, creds models.SignInUser) (string, error)
	GetUserIDFromToken(ctx context.Context, token string) (string, error)
	GetUserByID(ctx context.Context, id string) (*models.User, error)
	GetUserByUsername(ctx context.Context, username string) (*models.User, error)
	GetBlockedUserIDs(ctx context.Context, userID string) ([]string, error)
	BlockUser(ctx context.Context, userID, blockedUsername string) error
}

func NewUserService(userRepository repository.User, activityService Activity, jwtHandler token.JWTHandler) User {
	return &userService{
		repo:           userRepository,
		activityLogger: activityService,
		jwtHandler:     jwtHandler,
	}
}

func (service *userService) CreateNewUser(ctx context.Context, creds models.NewUser) (string, error) {
	err := creds.HashPassword()
	if err != nil {
		zap.L().Error("Error hashing password", zap.Error(err))
	}

	user, err := service.repo.CreateNewUniqueUser(ctx, creds)
	if err != nil {
		if errors.Is(err, models.ErrUserAlreadyExists) {
			zap.L().Error("Could not register new user with an existing username", zap.Error(err))
			return "", err
		}

		zap.L().Error("Error creating new user", zap.Error(err))
		return "", err
	}

	err = service.activityLogger.LogActivity(ctx, models.Activity{
		Type:   models.ActivityRegister,
		UserID: user.ID,
		Time:   time.Now(),
	})
	if err != nil {
		zap.L().Error("Error logging register activity", zap.Error(err))
	}

	token, err := service.jwtHandler.GenerateTokenForUser(user.ID)
	if err != nil {
		zap.L().Error("Error generating token for user", zap.Error(err))

		return "", err
	}

	return token, nil
}

func (service *userService) AuthenticateUser(ctx context.Context, creds models.SignInUser) (string, error) {
	user, err := service.repo.GetUserByUsername(ctx, creds.Username)
	if err != nil {
		if errors.Is(err, models.ErrUserDoesNotExist) {
			zap.L().Error("Could not find user with given username", zap.Error(err))

			return "", err
		}

		zap.L().Error("Error getting user", zap.Error(err))
		return "", err
	}

	err = creds.ComparePassword(user.Password)
	if err != nil {
		if errors.Is(err, models.ErrWrongPassword) {
			zap.L().Error("Password does not match", zap.Error(err))

			service.activityLogger.LogActivity(ctx, models.Activity{
				Type:   models.ActivityLogin,
				UserID: user.ID,
				Time:   time.Now(),
				Details: map[string]interface{}{
					"success": false,
				},
			})

			return "", err
		}

		zap.L().Error("Error comparing password", zap.Error(err))
		return "", err
	}

	token, err := service.jwtHandler.GenerateTokenForUser(user.ID)
	if err != nil {
		zap.L().Error("Error generating token for user", zap.Error(err))

		return "", err
	}

	err = service.activityLogger.LogActivity(ctx, models.Activity{
		Type:   models.ActivityLogin,
		UserID: user.ID,
		Time:   time.Now(),
		Details: map[string]interface{}{
			"success": true,
		},
	})
	if err != nil {
		zap.L().Error("Error logging login activity", zap.Error(err))
	}

	return token, nil
}

func (service *userService) GetUserIDFromToken(ctx context.Context, token string) (string, error) {
	userID, err := service.jwtHandler.ParseAuthorization(token)
	if err != nil {
		zap.L().Error("Error getting user ID from token", zap.Error(err))

		return "", err
	}

	return userID, nil
}

func (service *userService) GetUserByID(ctx context.Context, id string) (*models.User, error) {
	user, err := service.repo.GetUserByID(ctx, id)
	if err != nil {
		if errors.Is(err, models.ErrUserDoesNotExist) {
			zap.L().Error("Could not find user with given ID", zap.Error(err))

			return nil, err
		}

		zap.L().Error("Error getting user", zap.Error(err))
		return nil, err
	}

	return user, nil
}

func (service *userService) GetUserByUsername(ctx context.Context, username string) (*models.User, error) {
	user, err := service.repo.GetUserByUsername(ctx, username)
	if err != nil {
		if errors.Is(err, models.ErrUserDoesNotExist) {
			zap.L().Error("Could not find user with given username", zap.Error(err))

			return nil, err
		}

		zap.L().Error("Error getting user", zap.Error(err))
		return nil, err
	}

	return user, nil
}

func (service *userService) GetBlockedUserIDs(ctx context.Context, userID string) ([]string, error) {
	blockedIDs, err := service.repo.GetBlockedUserIDs(ctx, userID)
	if err != nil {
		zap.L().Error("Error getting blocked users", zap.Error(err))

		return nil, err
	}

	return blockedIDs, nil
}

func (service *userService) BlockUser(ctx context.Context, userID, blockedUsername string) error {
	blockedUser, err := service.repo.GetUserByUsername(ctx, blockedUsername)
	if err != nil {
		zap.L().Error("Error getting blocked user ID", zap.Error(err))

		return err
	}

	err = service.repo.BlockUser(ctx, userID, blockedUser.ID)
	if err != nil {
		zap.L().Error("Error blocking user", zap.Error(err))

		return err
	}

	service.activityLogger.LogActivity(ctx, models.Activity{
		Type:   models.ActivityBlock,
		UserID: userID,
		Time:   time.Now(),
		Details: map[string]interface{}{
			"blocked_user": blockedUsername,
		},
	})

	return nil
}
