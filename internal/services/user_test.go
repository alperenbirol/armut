package services_test

import (
	"context"
	"fmt"
	"messaging_service/internal/mocks/mockactivityservice"
	"messaging_service/internal/mocks/mockjwthandler"
	"messaging_service/internal/mocks/mockuserrepository"
	"messaging_service/internal/models"
	"messaging_service/internal/services"
	"testing"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
)

type UserServiceTestSuite struct {
	suite.Suite

	service services.User

	mockActivityService *mockactivityservice.Activity
	mockRepo            *mockuserrepository.User
	mockJWTHandler      *mockjwthandler.JWTHandler
}

func TestUserServiceTestSuite(t *testing.T) {
	suite.Run(t, new(UserServiceTestSuite))
}

func (suite *UserServiceTestSuite) SetupTest() {
	suite.mockActivityService = &mockactivityservice.Activity{}
	suite.mockRepo = &mockuserrepository.User{}
	suite.mockJWTHandler = &mockjwthandler.JWTHandler{}

	suite.service = services.NewUserService(suite.mockRepo, suite.mockActivityService, suite.mockJWTHandler)
}

func (suite *UserServiceTestSuite) TeardownTest() {
	suite.mockRepo.AssertExpectations(suite.T())
	suite.mockActivityService.AssertExpectations(suite.T())
}

var newUserParams = models.NewUser{
	User: models.User{
		Username: "test",
		Password: "test",
	},
}

var signInParams = models.SignInUser{
	User: models.User{
		Username: "test",
		Password: "test",
	},
}

func (suite *UserServiceTestSuite) TestCreateUser() {
	suite.mockRepo.EXPECT().CreateNewUniqueUser(mock.Anything, mock.Anything).Return(&models.User{
		ID: "123",
	}, nil)

	suite.mockActivityService.EXPECT().LogActivity(mock.Anything, mock.Anything).Return(nil)

	suite.mockJWTHandler.EXPECT().GenerateTokenForUser("123").Return("test", nil)

	token, err := suite.service.CreateNewUser(context.Background(), newUserParams)
	suite.NoError(err)

	suite.Equal("test", token)
}

func (suite *UserServiceTestSuite) TestCreateUserRepositoryFailure() {
	suite.mockRepo.EXPECT().CreateNewUniqueUser(mock.Anything, mock.Anything).Return(nil, fmt.Errorf("some error"))

	token, err := suite.service.CreateNewUser(context.Background(), newUserParams)
	suite.Error(err)

	suite.Empty(token)
}

func (suite *UserServiceTestSuite) TestCreateUserDuplicateUser() {
	suite.mockRepo.EXPECT().CreateNewUniqueUser(mock.Anything, mock.Anything).Return(nil, models.ErrUserAlreadyExists)

	token, err := suite.service.CreateNewUser(context.Background(), newUserParams)
	suite.ErrorIs(err, models.ErrUserAlreadyExists)

	suite.Empty(token)
}

func (suite *UserServiceTestSuite) TestCreateUserActivityServiceFailure() {
	suite.mockRepo.EXPECT().CreateNewUniqueUser(mock.Anything, mock.Anything).Return(&models.User{
		ID: "123",
	}, nil)

	suite.mockActivityService.EXPECT().LogActivity(mock.Anything, mock.Anything).Return(fmt.Errorf("some error"))

	suite.mockJWTHandler.EXPECT().GenerateTokenForUser("123").Return("test", nil)

	token, err := suite.service.CreateNewUser(context.Background(), newUserParams)
	suite.NoError(err)

	suite.Equal("test", token)
}

func (suite *UserServiceTestSuite) TestCreateUserJWTHandlerFailure() {
	suite.mockRepo.EXPECT().CreateNewUniqueUser(mock.Anything, mock.Anything).Return(&models.User{
		ID: "123",
	}, nil)

	suite.mockActivityService.EXPECT().LogActivity(mock.Anything, mock.Anything).Return(nil)

	suite.mockJWTHandler.EXPECT().GenerateTokenForUser("123").Return("", fmt.Errorf("some error"))

	token, err := suite.service.CreateNewUser(context.Background(), newUserParams)
	suite.Error(err)

	suite.Empty(token)
}

func (suite *UserServiceTestSuite) TestLoginUser() {
	suite.mockRepo.EXPECT().GetUserByUsername(mock.Anything, mock.Anything).Return(&models.User{
		ID:       "123",
		Username: "test",
		Password: "$2a$10$C05nZnOOOjbSiAke2fHrre8w3eHo/EdD8dNnJMmOA2LgCTDw/giCe",
	}, nil)

	suite.mockJWTHandler.EXPECT().GenerateTokenForUser("123").Return("test", nil)

	suite.mockActivityService.EXPECT().LogActivity(mock.Anything, mock.Anything).Return(nil)

	token, err := suite.service.AuthenticateUser(context.Background(), signInParams)
	suite.NoError(err)

	suite.Equal("test", token)
}

func (suite *UserServiceTestSuite) TestLoginUserRepositoryFailure() {
	suite.mockRepo.EXPECT().GetUserByUsername(mock.Anything, mock.Anything).Return(nil, fmt.Errorf("some error"))

	token, err := suite.service.AuthenticateUser(context.Background(), signInParams)
	suite.Error(err)

	suite.Empty(token)
}

func (suite *UserServiceTestSuite) TestLoginUserUserDoesNotExist() {
	suite.mockRepo.EXPECT().GetUserByUsername(mock.Anything, mock.Anything).Return(nil, models.ErrUserDoesNotExist)

	token, err := suite.service.AuthenticateUser(context.Background(), signInParams)
	suite.ErrorIs(err, models.ErrUserDoesNotExist)

	suite.Empty(token)
}

func (suite *UserServiceTestSuite) TestLoginUserIncorrectPassword() {
	suite.mockRepo.EXPECT().GetUserByUsername(mock.Anything, mock.Anything).Return(&models.User{
		ID:       "123",
		Username: "test",
		Password: "$2a$10$7vKDV1aScvytsaI9WssJiu.sKMv3.WHHbkX.Holtpt0.vJOl.rIYi",
	}, nil)

	suite.mockActivityService.EXPECT().LogActivity(mock.Anything, mock.Anything).Return(nil)

	token, err := suite.service.AuthenticateUser(context.Background(), signInParams)
	suite.ErrorIs(err, models.ErrWrongPassword)

	suite.Empty(token)
}

func (suite *UserServiceTestSuite) TestLoginUserJWTFailure() {
	suite.mockRepo.EXPECT().GetUserByUsername(mock.Anything, mock.Anything).Return(&models.User{
		ID:       "123",
		Username: "test",
		Password: "$2a$10$C05nZnOOOjbSiAke2fHrre8w3eHo/EdD8dNnJMmOA2LgCTDw/giCe",
	}, nil)

	suite.mockJWTHandler.EXPECT().GenerateTokenForUser("123").Return("", fmt.Errorf("some error"))

	token, err := suite.service.AuthenticateUser(context.Background(), signInParams)
	suite.Error(err)

	suite.Empty(token)
}

func (suite *UserServiceTestSuite) TestLoginUserLogActivityFailure() {
	suite.mockRepo.EXPECT().GetUserByUsername(mock.Anything, mock.Anything).Return(&models.User{
		ID:       "123",
		Username: "test",
		Password: "$2a$10$C05nZnOOOjbSiAke2fHrre8w3eHo/EdD8dNnJMmOA2LgCTDw/giCe",
	}, nil)

	suite.mockJWTHandler.EXPECT().GenerateTokenForUser("123").Return("test", nil)

	suite.mockActivityService.EXPECT().LogActivity(mock.Anything, mock.Anything).Return(fmt.Errorf("some error"))

	token, err := suite.service.AuthenticateUser(context.Background(), signInParams)
	suite.NoError(err)

	suite.Equal("test", token)
}

func (suite *UserServiceTestSuite) TestGetUserIdFromToken() {
	suite.mockJWTHandler.EXPECT().ParseAuthorization("test").Return("123", nil)

	id, err := suite.service.GetUserIDFromToken(context.Background(), "test")
	suite.NoError(err)

	suite.Equal("123", id)
}

func (suite *UserServiceTestSuite) TestGetUserIdFromTokenJWTHandlerFailure() {
	suite.mockJWTHandler.EXPECT().ParseAuthorization("test").Return("", fmt.Errorf("some error"))

	id, err := suite.service.GetUserIDFromToken(context.Background(), "test")
	suite.Error(err)

	suite.Empty(id)
}

func (suite *UserServiceTestSuite) TestGetUserByID() {
	suite.mockRepo.EXPECT().GetUserByID(mock.Anything, "123").Return(&models.User{
		ID:       "123",
		Username: "test",
	}, nil)

	user, err := suite.service.GetUserByID(context.Background(), "123")
	suite.NoError(err)

	suite.Equal("test", user.Username)
}

func (suite *UserServiceTestSuite) TestGetUserByIDRepositoryFailure() {
	suite.mockRepo.EXPECT().GetUserByID(mock.Anything, "123").Return(nil, fmt.Errorf("some error"))

	user, err := suite.service.GetUserByID(context.Background(), "123")
	suite.Error(err)

	suite.Nil(user)
}

func (suite *UserServiceTestSuite) TestGetUserByIDUserDoesNotExist() {
	suite.mockRepo.EXPECT().GetUserByID(mock.Anything, "123").Return(nil, models.ErrUserDoesNotExist)

	user, err := suite.service.GetUserByID(context.Background(), "123")
	suite.ErrorIs(err, models.ErrUserDoesNotExist)

	suite.Nil(user)
}

func (suite *UserServiceTestSuite) TestGetUserByUsername() {
	suite.mockRepo.EXPECT().GetUserByUsername(mock.Anything, "test").Return(&models.User{
		ID:       "123",
		Username: "test",
	}, nil)

	user, err := suite.service.GetUserByUsername(context.Background(), "test")
	suite.NoError(err)

	suite.Equal("123", user.ID)
}

func (suite *UserServiceTestSuite) TestGetUserByUsernameRepositoryFailure() {
	suite.mockRepo.EXPECT().GetUserByUsername(mock.Anything, "test").Return(nil, fmt.Errorf("some error"))

	user, err := suite.service.GetUserByUsername(context.Background(), "test")
	suite.Error(err)

	suite.Nil(user)
}

func (suite *UserServiceTestSuite) TestGetUserByUsernameUserDoesNotExist() {
	suite.mockRepo.EXPECT().GetUserByUsername(mock.Anything, "test").Return(nil, models.ErrUserDoesNotExist)

	user, err := suite.service.GetUserByUsername(context.Background(), "test")
	suite.ErrorIs(err, models.ErrUserDoesNotExist)

	suite.Nil(user)
}

func (suite *UserServiceTestSuite) TestGetBlockedUserIDs() {
	suite.mockRepo.EXPECT().GetBlockedUserIDs(mock.Anything, "123").Return([]string{"456"}, nil)

	blockedIDs, err := suite.service.GetBlockedUserIDs(context.Background(), "123")
	suite.NoError(err)

	suite.Equal([]string{"456"}, blockedIDs)
}

func (suite *UserServiceTestSuite) TestGetBlockedUserIDsRepositoryFailure() {
	suite.mockRepo.EXPECT().GetBlockedUserIDs(mock.Anything, "123").Return(nil, fmt.Errorf("some error"))

	blockedIDs, err := suite.service.GetBlockedUserIDs(context.Background(), "123")
	suite.Error(err)

	suite.Nil(blockedIDs)
}

func (suite *UserServiceTestSuite) TestBlockUser() {
	suite.mockRepo.EXPECT().GetUserByUsername(mock.Anything, "blocked_user").Return(&models.User{
		ID:       "456",
		Username: "blocked_user",
	}, nil)

	suite.mockRepo.EXPECT().BlockUser(mock.Anything, "123", "456").Return(nil)

	suite.mockActivityService.EXPECT().LogActivity(mock.Anything, mock.Anything).Return(nil)

	err := suite.service.BlockUser(context.Background(), "123", "blocked_user")
	suite.NoError(err)
}

func (suite *UserServiceTestSuite) TestBlockUserGetUserByUsernameFailure() {
	suite.mockRepo.EXPECT().GetUserByUsername(mock.Anything, "blocked_user").Return(nil, fmt.Errorf("some error"))

	err := suite.service.BlockUser(context.Background(), "123", "blocked_user")
	suite.Error(err)
}

func (suite *UserServiceTestSuite) TestBlockUserRepositoryFailure() {
	suite.mockRepo.EXPECT().GetUserByUsername(mock.Anything, "blocked_user").Return(&models.User{
		ID:       "456",
		Username: "blocked_user",
	}, nil)

	suite.mockRepo.EXPECT().BlockUser(mock.Anything, "123", "456").Return(fmt.Errorf("some error"))

	err := suite.service.BlockUser(context.Background(), "123", "blocked_user")
	suite.Error(err)
}
