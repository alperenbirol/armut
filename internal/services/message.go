package services

import (
	"context"
	"messaging_service/internal/models"
	"messaging_service/internal/repository"
	"time"

	"go.uber.org/zap"
	"golang.org/x/exp/slices"
)

type messageService struct {
	repo repository.Message

	userService     User
	activityService Activity
}

//go:generate mockery --name Message --output ../mocks/mockmessageservice --outpkg mockmessageservice --case underscore --with-expecter
type Message interface {
	GetUserMessages(ctx context.Context, userID string) ([]models.MessageResponse, error)
	SendMessage(ctx context.Context, params models.MessageRequest) error
}

func NewMessageService(messageRepository repository.Message, userService User, activityService Activity) Message {
	return &messageService{
		repo:            messageRepository,
		userService:     userService,
		activityService: activityService,
	}
}

func (s *messageService) GetUserMessages(ctx context.Context, userID string) ([]models.MessageResponse, error) {
	messages, err := s.repo.GetUserMessages(ctx, userID)
	if err != nil {
		zap.L().Error("failed to get user messages", zap.Error(err))

		return nil, err
	}

	if len(messages) == 0 {
		return nil, models.ErrNoMessages
	}

	receiverUser, err := s.userService.GetUserByID(ctx, messages[0].ReceiverID)
	if err != nil {
		return nil, err
	}

	userIDtoUsername := map[string]string{
		messages[0].ReceiverID: receiverUser.Username,
	}

	var messagesResponse = make([]models.MessageResponse, len(messages))
	for i, message := range messages {
		senderUsername, ok := userIDtoUsername[message.SenderID]
		if !ok {
			senderUser, err := s.userService.GetUserByID(ctx, message.SenderID)
			if err != nil {
				return nil, err
			}

			senderUsername = senderUser.Username
		}
		messagesResponse[i] = models.MessageResponse{
			SenderUsername:   senderUsername,
			ReceiverUsername: receiverUser.Username,
			Content:          message.Content,
			Time:             message.Time,
		}
	}

	return messagesResponse, nil
}

func (s *messageService) SendMessage(ctx context.Context, params models.MessageRequest) error {
	receiverUser, err := s.userService.GetUserByUsername(ctx, params.ReceiverUsername)
	if err != nil {
		return err
	}

	if receiverUser.ID == params.SenderID {
		return models.ErrSameSenderReceiver
	}

	blockedUserIDs, err := s.userService.GetBlockedUserIDs(ctx, receiverUser.ID)
	if err != nil {
		return err
	}

	if slices.Contains(blockedUserIDs, params.SenderID) {
		s.activityService.LogActivity(ctx, models.Activity{
			Type:   models.ActivitySendMsg,
			UserID: params.SenderID,
			Time:   time.Now(),
			Details: map[string]interface{}{
				"receiver": receiverUser.Username,
				"blocked":  true,
			},
		})

		return models.ErrSenderIsBlocked
	}

	now := time.Now()

	message := models.Message{
		SenderID:   params.SenderID,
		ReceiverID: receiverUser.ID,
		Content:    params.Content,
		Time:       now,
	}

	err = s.repo.SaveMessage(ctx, message)
	if err != nil {
		zap.L().Error("failed to save message", zap.Error(err))

		return err
	}

	s.activityService.LogActivity(ctx, models.Activity{
		Type:    models.ActivitySendMsg,
		UserID:  params.SenderID,
		Time:    now,
		Details: map[string]interface{}{"receiver": receiverUser.Username},
	})

	return nil
}
