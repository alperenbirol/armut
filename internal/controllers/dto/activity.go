package dto

import (
	"encoding/json"
	"messaging_service/internal/models"
)

type ActivityResponse struct {
	Type      string
	Timestamp int64
	Details   map[string]interface{}
}

func (a *ActivityResponse) MarshalJSON() ([]byte, error) {
	inlined := make(map[string]interface{})

	for k, v := range a.Details {
		inlined[k] = v
	}

	inlined["event_type"] = a.Type
	inlined["timestamp"] = a.Timestamp

	return json.Marshal(inlined)
}

func ActivityFromModel(model models.Activity) ActivityResponse {
	return ActivityResponse{
		Type:      string(model.Type),
		Timestamp: model.Time.Unix(),
		Details:   model.Details,
	}
}
