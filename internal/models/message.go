package models

import (
	"errors"
	"time"
)

type Message struct {
	SenderID   string
	ReceiverID string
	Content    string
	Time       time.Time
}

type MessageRequest struct {
	SenderID         string
	ReceiverUsername string
	Content          string
}

type MessageResponse struct {
	SenderUsername   string
	ReceiverUsername string
	Content          string
	Time             time.Time
}

var (
	ErrNoMessages         = errors.New("no messages")
	ErrSameSenderReceiver = errors.New("sender and receiver are the same")
	ErrSenderIsBlocked    = errors.New("receiver has blocked the sender")
)
