// Code generated by mockery v2.20.0. DO NOT EDIT.

package mockmessagerepository

import (
	context "context"
	models "messaging_service/internal/models"

	mock "github.com/stretchr/testify/mock"
)

// Message is an autogenerated mock type for the Message type
type Message struct {
	mock.Mock
}

type Message_Expecter struct {
	mock *mock.Mock
}

func (_m *Message) EXPECT() *Message_Expecter {
	return &Message_Expecter{mock: &_m.Mock}
}

// GetUserMessages provides a mock function with given fields: ctx, userID
func (_m *Message) GetUserMessages(ctx context.Context, userID string) ([]models.Message, error) {
	ret := _m.Called(ctx, userID)

	var r0 []models.Message
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, string) ([]models.Message, error)); ok {
		return rf(ctx, userID)
	}
	if rf, ok := ret.Get(0).(func(context.Context, string) []models.Message); ok {
		r0 = rf(ctx, userID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]models.Message)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, userID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Message_GetUserMessages_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'GetUserMessages'
type Message_GetUserMessages_Call struct {
	*mock.Call
}

// GetUserMessages is a helper method to define mock.On call
//   - ctx context.Context
//   - userID string
func (_e *Message_Expecter) GetUserMessages(ctx interface{}, userID interface{}) *Message_GetUserMessages_Call {
	return &Message_GetUserMessages_Call{Call: _e.mock.On("GetUserMessages", ctx, userID)}
}

func (_c *Message_GetUserMessages_Call) Run(run func(ctx context.Context, userID string)) *Message_GetUserMessages_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(string))
	})
	return _c
}

func (_c *Message_GetUserMessages_Call) Return(_a0 []models.Message, _a1 error) *Message_GetUserMessages_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *Message_GetUserMessages_Call) RunAndReturn(run func(context.Context, string) ([]models.Message, error)) *Message_GetUserMessages_Call {
	_c.Call.Return(run)
	return _c
}

// SaveMessage provides a mock function with given fields: ctx, params
func (_m *Message) SaveMessage(ctx context.Context, params models.Message) error {
	ret := _m.Called(ctx, params)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, models.Message) error); ok {
		r0 = rf(ctx, params)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Message_SaveMessage_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'SaveMessage'
type Message_SaveMessage_Call struct {
	*mock.Call
}

// SaveMessage is a helper method to define mock.On call
//   - ctx context.Context
//   - params models.Message
func (_e *Message_Expecter) SaveMessage(ctx interface{}, params interface{}) *Message_SaveMessage_Call {
	return &Message_SaveMessage_Call{Call: _e.mock.On("SaveMessage", ctx, params)}
}

func (_c *Message_SaveMessage_Call) Run(run func(ctx context.Context, params models.Message)) *Message_SaveMessage_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(models.Message))
	})
	return _c
}

func (_c *Message_SaveMessage_Call) Return(_a0 error) *Message_SaveMessage_Call {
	_c.Call.Return(_a0)
	return _c
}

func (_c *Message_SaveMessage_Call) RunAndReturn(run func(context.Context, models.Message) error) *Message_SaveMessage_Call {
	_c.Call.Return(run)
	return _c
}

type mockConstructorTestingTNewMessage interface {
	mock.TestingT
	Cleanup(func())
}

// NewMessage creates a new instance of Message. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewMessage(t mockConstructorTestingTNewMessage) *Message {
	mock := &Message{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
