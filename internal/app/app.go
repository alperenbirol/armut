package app

import (
	"fmt"
	"messaging_service/internal/controllers"
	"messaging_service/internal/middlewares"
	"messaging_service/internal/router"

	"github.com/gofiber/fiber/v2"
)

type App struct {
	Authenticator      middlewares.Authenticator
	UserController     controllers.User
	ActivityController controllers.Activity
	MessageController  controllers.Message
}

func (app *App) Run(port uint) error {
	fiberApp := fiber.New()

	router := router.NewRouter(router.RouterParams{
		FiberApp:           fiberApp,
		Authenticator:      app.Authenticator,
		UserController:     app.UserController,
		ActivityController: app.ActivityController,
		MessageController:  app.MessageController,
	})
	router.RegisterRoutes()

	return fiberApp.Listen(fmt.Sprintf(":%d", port))
}
