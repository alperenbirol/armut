package dto_test

import (
	"fmt"
	"messaging_service/internal/controllers/dto"
	"messaging_service/internal/models"
	"testing"
	"time"
)

func TestActivityResponse_MarshalJSON(t *testing.T) {
	now := time.Now()
	expected := fmt.Sprintf(`{"event_type":"%s","foo":"bar","timestamp":%d}`, models.ActivityLogin, now.Unix())

	activity := dto.ActivityResponse{
		Type:      string(models.ActivityLogin),
		Timestamp: now.Unix(),
		Details: map[string]interface{}{
			"foo": "bar",
		},
	}

	actual, err := activity.MarshalJSON()
	if err != nil {
		t.Fatal(err)
	}

	if string(actual) != expected {
		t.Fatalf("expected %s, got %s", expected, string(actual))
	}
}
