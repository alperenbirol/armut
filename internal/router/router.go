package router

import (
	"messaging_service/internal/controllers"
	"messaging_service/internal/middlewares"

	"github.com/gofiber/fiber/v2"
)

type router struct {
	*fiber.App
	authenticator      middlewares.Authenticator
	userController     controllers.User
	activityController controllers.Activity
	messageController  controllers.Message
}

type RouterParams struct {
	FiberApp           *fiber.App
	Authenticator      middlewares.Authenticator
	UserController     controllers.User
	ActivityController controllers.Activity
	MessageController  controllers.Message
}

func NewRouter(params RouterParams) *router {
	return &router{
		App:                params.FiberApp,
		authenticator:      params.Authenticator,
		userController:     params.UserController,
		activityController: params.ActivityController,
		messageController:  params.MessageController,
	}
}

func (r *router) RegisterRoutes() {
	r.registerUserRoutes()

	r.registerAuthenticatedRoutes()
}

func (router *router) registerUserRoutes() {
	userRouter := router.Group("/user")

	userRouter.Post("/register", router.userController.Register)
	userRouter.Post("/login", router.userController.Login)
}

func (router *router) registerAuthenticatedRoutes() {
	authenticatedRouter := router.Group("/")

	authenticatedRouter.Use(router.authenticator.Authenticate)

	router.registerActivityRoutes()
	router.registerMessageRoutes()

	router.Post("/block", router.userController.BlockUser)
}

func (router *router) registerActivityRoutes() {
	activityRouter := router.Group("/activity")

	activityRouter.Get("/", router.activityController.GetActivity)
}

func (router *router) registerMessageRoutes() {
	messageRouter := router.Group("/message")

	messageRouter.Get("/", router.messageController.GetMessages)
	messageRouter.Post("/", router.messageController.SendMessage)
}
