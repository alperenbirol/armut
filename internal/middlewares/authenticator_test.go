package middlewares_test

import (
	"messaging_service/internal/middlewares"
	"messaging_service/internal/mocks/mockuserservice"
	"net/http/httptest"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
)

var user = struct {
	ID    string
	Token string
}{
	ID:    "640807a2635a939d12f01bd3",
	Token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NzgzMzQyODcsImlhdCI6MTY3ODI0Nzg4Nywic3ViIjoiNjQwODA3YTI2MzVhOTM5ZDEyZjAxYmQzIn0.Uv2INhau8M0Ka6PWQ4zxpoC0R1WePZNDlH5X-1LIObs",
}

type AuthorizationMiddlewareTestSuite struct {
	suite.Suite

	mockUserService *mockuserservice.User
	router          *fiber.App
}

func TestAuthorizationMiddlewareTestSuite(t *testing.T) {
	suite.Run(t, new(AuthorizationMiddlewareTestSuite))
}

func (suite *AuthorizationMiddlewareTestSuite) SetupTest() {
	suite.mockUserService = new(mockuserservice.User)

	authMiddleware := middlewares.NewAuthenticator(suite.mockUserService)

	suite.router = fiber.New()

	suite.router.Use(authMiddleware.Authenticate)

	suite.router.Get("/test", func(c *fiber.Ctx) error {
		return c.SendStatus(fiber.StatusOK)
	})
}

func (suite *AuthorizationMiddlewareTestSuite) TeardownTest() {
	suite.mockUserService.AssertExpectations(suite.T())
}

func (suite *AuthorizationMiddlewareTestSuite) TestAuthorizationMiddleware() {
	suite.mockUserService.EXPECT().GetUserIDFromToken(mock.Anything, user.Token).Return(user.ID, nil)

	req := httptest.NewRequest("GET", "/test", nil)
	req.Header.Set("Authorization", user.Token)

	res, err := suite.router.Test(req)
	suite.NoError(err)

	suite.Equal(fiber.StatusOK, res.StatusCode)
}

func (suite *AuthorizationMiddlewareTestSuite) TestAuthorizationMiddlewareWithoutToken() {
	req := httptest.NewRequest("GET", "/test", nil)

	res, err := suite.router.Test(req)
	suite.NoError(err)

	suite.Equal(fiber.StatusUnauthorized, res.StatusCode)
}

func (suite *AuthorizationMiddlewareTestSuite) TestAuthorizationMiddlewareWithInvalidToken() {
	suite.mockUserService.EXPECT().GetUserIDFromToken(mock.Anything, user.Token).Return("", fiber.ErrUnauthorized)

	req := httptest.NewRequest("GET", "/test", nil)
	req.Header.Set("Authorization", user.Token)

	res, err := suite.router.Test(req)
	suite.NoError(err)

	suite.Equal(fiber.StatusUnauthorized, res.StatusCode)
}
