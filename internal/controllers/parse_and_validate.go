package controllers

import (
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"
)

type RequestParser struct {
	Validator *validator.Validate
}

func (parser *RequestParser) parseAndValidate(ctx *fiber.Ctx, request any) error {
	err := ctx.BodyParser(&request)
	if err != nil {
		zap.L().Error("Error parsing request", zap.Error(err))

		return err
	}

	err = parser.Validator.Struct(request)
	if err != nil {
		zap.L().Sugar().Error("Could not validate request", zap.Error(err))

		return err
	}

	return nil
}
