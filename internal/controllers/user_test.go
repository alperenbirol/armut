package controllers_test

import (
	"fmt"
	"io"
	"messaging_service/internal/controllers"
	"messaging_service/internal/mocks/mockuserservice"
	"messaging_service/internal/models"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
)

type UserControllersTestSuite struct {
	suite.Suite

	mockUserService *mockuserservice.User
	router          *fiber.App
}

func TestUserControllersTestSuite(t *testing.T) {
	suite.Run(t, new(UserControllersTestSuite))
}

func (suite *UserControllersTestSuite) SetupTest() {
	suite.mockUserService = new(mockuserservice.User)

	requestParser := controllers.RequestParser{
		validator.New(),
	}

	ctrl := controllers.NewUserController(requestParser, suite.mockUserService)

	suite.router = fiber.New()

	suite.router.Use(func(c *fiber.Ctx) error {
		c.Locals("userID", "testUser")

		return c.Next()
	})

	userRouter := suite.router.Group("/user")

	userRouter.Post("/register", ctrl.Register)
	userRouter.Post("/login", ctrl.Login)
	userRouter.Post("/block", ctrl.BlockUser)
}

func (suite *UserControllersTestSuite) TeardownTest() {
	suite.mockUserService.AssertExpectations(suite.T())
}

func (suite *UserControllersTestSuite) TestRegister() {
	suite.mockUserService.EXPECT().CreateNewUser(mock.Anything, models.NewUser{
		User: models.User{
			Username: "testUser",
			Password: "testPassword",
		},
	}).Return("token", nil)

	req := httptest.NewRequest("POST", "/user/register", strings.NewReader(`{"username": "testUser", "password": "testPassword"}`))
	req.Header.Set("Content-Type", "application/json")

	resp, err := suite.router.Test(req)
	suite.NoError(err)

	suite.Equal(http.StatusCreated, resp.StatusCode)

	body, err := io.ReadAll(resp.Body)
	suite.NoError(err)

	suite.Equal(`{"access_token":"token"}`, string(body))
}

func (suite *UserControllersTestSuite) TestRegisterServiceFailure() {
	suite.mockUserService.EXPECT().CreateNewUser(mock.Anything, mock.Anything).Return("", fmt.Errorf("test error"))

	req := httptest.NewRequest("POST", "/user/register", strings.NewReader(`{"username": "testUser", "password": "testPassword"}`))
	req.Header.Set("Content-Type", "application/json")

	resp, err := suite.router.Test(req)
	suite.NoError(err)

	suite.Equal(http.StatusInternalServerError, resp.StatusCode)
}

func (suite *UserControllersTestSuite) TestRegisterInvalidRequest() {
	req := httptest.NewRequest("POST", "/user/register", strings.NewReader(`{"username": "testUser"}`))
	req.Header.Set("Content-Type", "application/json")

	resp, err := suite.router.Test(req)
	suite.NoError(err)

	suite.Equal(http.StatusBadRequest, resp.StatusCode)
}

func (suite *UserControllersTestSuite) TestRegisterUsernameTaken() {
	suite.mockUserService.EXPECT().CreateNewUser(mock.Anything, mock.Anything).Return("", models.ErrUserAlreadyExists)

	req := httptest.NewRequest("POST", "/user/register", strings.NewReader(`{"username": "testUser", "password": "testPassword"}`))
	req.Header.Set("Content-Type", "application/json")

	resp, err := suite.router.Test(req)
	suite.NoError(err)

	suite.Equal(http.StatusConflict, resp.StatusCode)
}

func (suite *UserControllersTestSuite) TestLogin() {
	suite.mockUserService.EXPECT().AuthenticateUser(mock.Anything, models.SignInUser{
		User: models.User{
			Username: "testUser",
			Password: "testPassword",
		},
	}).Return("token", nil)

	req := httptest.NewRequest("POST", "/user/login", strings.NewReader(`{"username": "testUser", "password": "testPassword"}`))
	req.Header.Set("Content-Type", "application/json")

	resp, err := suite.router.Test(req)
	suite.NoError(err)

	suite.Equal(http.StatusOK, resp.StatusCode)

	body, err := io.ReadAll(resp.Body)
	suite.NoError(err)

	suite.Equal(`{"access_token":"token"}`, string(body))
}

func (suite *UserControllersTestSuite) TestLoginServiceFailure() {
	suite.mockUserService.EXPECT().AuthenticateUser(mock.Anything, mock.Anything).Return("", fmt.Errorf("test error"))

	req := httptest.NewRequest("POST", "/user/login", strings.NewReader(`{"username": "testUser", "password": "testPassword"}`))
	req.Header.Set("Content-Type", "application/json")

	resp, err := suite.router.Test(req)
	suite.NoError(err)

	suite.Equal(http.StatusInternalServerError, resp.StatusCode)
}

func (suite *UserControllersTestSuite) TestLoginInvalidRequest() {
	req := httptest.NewRequest("POST", "/user/login", strings.NewReader(`{"username": "testUser"}`))
	req.Header.Set("Content-Type", "application/json")

	resp, err := suite.router.Test(req)
	suite.NoError(err)

	suite.Equal(http.StatusBadRequest, resp.StatusCode)
}

func (suite *UserControllersTestSuite) TestLoginInvalidPassword() {
	suite.mockUserService.EXPECT().AuthenticateUser(mock.Anything, mock.Anything).Return("", models.ErrWrongPassword)

	req := httptest.NewRequest("POST", "/user/login", strings.NewReader(`{"username": "testUser", "password": "wrongPassword"}`))
	req.Header.Set("Content-Type", "application/json")

	resp, err := suite.router.Test(req)
	suite.NoError(err)

	suite.Equal(http.StatusUnauthorized, resp.StatusCode)

	body, err := io.ReadAll(resp.Body)
	suite.NoError(err)

	suite.Equal(`wrong password for user testUser`, string(body))
}

func (suite *UserControllersTestSuite) TestLoginUserNotFound() {
	suite.mockUserService.EXPECT().AuthenticateUser(mock.Anything, mock.Anything).Return("", models.ErrUserDoesNotExist)

	req := httptest.NewRequest("POST", "/user/login", strings.NewReader(`{"username": "notAUser", "password": "testPassword"}`))
	req.Header.Set("Content-Type", "application/json")

	resp, err := suite.router.Test(req)
	suite.NoError(err)

	suite.Equal(http.StatusNotFound, resp.StatusCode)

	body, err := io.ReadAll(resp.Body)
	suite.NoError(err)

	suite.Equal(`user notAUser does not exist`, string(body))
}

func (suite *UserControllersTestSuite) TestBlockUser() {
	suite.mockUserService.EXPECT().BlockUser(mock.Anything, "testUser", "blocked_user").Return(nil)

	req := httptest.NewRequest("POST", "/user/block", strings.NewReader(`{"username": "blocked_user"}`))
	req.Header.Set("Content-Type", "application/json")

	resp, err := suite.router.Test(req)
	suite.NoError(err)

	suite.Equal(http.StatusOK, resp.StatusCode)
}

func (suite *UserControllersTestSuite) TestBlockUserServiceFailure() {
	suite.mockUserService.EXPECT().BlockUser(mock.Anything, "testUser", "blocked_user").Return(fmt.Errorf("test error"))

	req := httptest.NewRequest("POST", "/user/block", strings.NewReader(`{"username": "blocked_user"}`))
	req.Header.Set("Content-Type", "application/json")

	resp, err := suite.router.Test(req)
	suite.NoError(err)

	suite.Equal(http.StatusInternalServerError, resp.StatusCode)
}

func (suite *UserControllersTestSuite) TestBlockUserInvalidRequest() {
	req := httptest.NewRequest("POST", "/user/block", strings.NewReader(`{"username": ""}`))
	req.Header.Set("Content-Type", "application/json")

	resp, err := suite.router.Test(req)
	suite.NoError(err)

	suite.Equal(http.StatusBadRequest, resp.StatusCode)
}
