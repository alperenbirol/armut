package token

import (
	"errors"
	"time"

	"github.com/golang-jwt/jwt"
)

const TOKEN_VALID_FOR = time.Hour * 24

var (
	ErrInvalidToken = errors.New("invalid token")
)

type jwtHandler struct {
	secret   []byte
	validFor time.Duration
}

//go:generate mockery --name JWTHandler --output ../mocks/mockjwthandler --outpkg mockjwthandler --case underscore --with-expecter
type JWTHandler interface {
	GenerateTokenForUser(userID string) (string, error)
	ParseAuthorization(signedTokenString string) (string, error)
}

func NewJWTHandler(secret string) JWTHandler {
	return &jwtHandler{
		secret:   []byte(secret),
		validFor: TOKEN_VALID_FOR,
	}
}

func (handler *jwtHandler) GenerateTokenForUser(userID string) (string, error) {
	claims := jwt.StandardClaims{
		ExpiresAt: time.Now().Add(handler.validFor).Unix(),
		IssuedAt:  time.Now().Unix(),
		Subject:   userID,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tokenString, err := token.SignedString(handler.secret)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func (handler *jwtHandler) parseToken(signedTokenString string) (*jwt.Token, error) {
	token, err := jwt.ParseWithClaims(signedTokenString, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		return handler.secret, nil
	})
	if err != nil {
		return nil, err
	}

	if !token.Valid {
		return nil, ErrInvalidToken
	}

	_, ok := token.Claims.(*jwt.StandardClaims)
	if token.Claims.Valid() != nil || !ok {
		return nil, errors.New("invalid token claims")
	}

	return token, nil
}

func (handler *jwtHandler) ParseAuthorization(signedTokenString string) (string, error) {
	token, err := handler.parseToken(signedTokenString)
	if err != nil {
		return "", err
	}

	claims := token.Claims.(*jwt.StandardClaims)

	return claims.Subject, nil
}
