package repository

import (
	"context"
	"messaging_service/internal/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type messageRepository struct {
	client *mongo.Client
}

//go:generate mockery --name Message --output ../mocks/mockmessagerepository --outpkg mockmessagerepository --case underscore --with-expecter
type Message interface {
	GetUserMessages(ctx context.Context, userID string) ([]models.Message, error)
	SaveMessage(ctx context.Context, params models.Message) error
}

func NewMongoMessageRepository(client *mongo.Client) Message {
	return &messageRepository{
		client: client,
	}
}

func (r *messageRepository) messagesCollection() *mongo.Collection {
	return r.client.Database("messaging_service").Collection("messages")
}

func (r *messageRepository) GetUserMessages(ctx context.Context, userID string) ([]models.Message, error) {
	userObjectID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		return nil, err
	}

	cur, err := r.messagesCollection().Find(ctx, bson.M{"receiver_id": userObjectID})
	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)

	var messages []models.Message
	for cur.Next(ctx) {
		var message message
		err := cur.Decode(&message)
		if err != nil {
			return nil, err
		}

		messages = append(messages, *message.toModel())
	}

	if err := cur.Err(); err != nil {
		return nil, err
	}

	return messages, nil
}

func (r *messageRepository) SaveMessage(ctx context.Context, params models.Message) error {
	var messageDTO message
	messageDTO.fromModel(params)

	_, err := r.messagesCollection().InsertOne(ctx, messageDTO)
	if err != nil {
		return err
	}

	return nil
}
