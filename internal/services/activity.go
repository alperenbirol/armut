package services

import (
	"context"
	"messaging_service/internal/models"
	"messaging_service/internal/repository"
)

type activityService struct {
	repo repository.Activity
}

//go:generate mockery --name Activity --output ../mocks/mockactivityservice --outpkg mockactivityservice --case underscore --with-expecter
type Activity interface {
	LogActivity(ctx context.Context, activity models.Activity) error
	GetActivity(ctx context.Context, userID string) ([]models.Activity, error)
}

func NewActivityService(activityRepository repository.Activity) Activity {
	return &activityService{
		repo: activityRepository,
	}
}

func (s *activityService) LogActivity(ctx context.Context, activity models.Activity) error {
	return s.repo.LogActivity(ctx, activity)
}

func (s *activityService) GetActivity(ctx context.Context, userID string) ([]models.Activity, error) {
	return s.repo.GetActivity(ctx, userID)
}
