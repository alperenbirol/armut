package repository

import (
	"context"
	"errors"
	"messaging_service/internal/models"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"
)

type userRepository struct {
	client *mongo.Client
}

//go:generate mockery --name User --output ../mocks/mockuserrepository --outpkg mockuserrepository --case underscore --with-expecter
type User interface {
	CreateNewUniqueUser(ctx context.Context, params models.NewUser) (*models.User, error)
	GetUserByUsername(ctx context.Context, username string) (*models.User, error)
	GetUserByID(ctx context.Context, id string) (*models.User, error)
	GetBlockedUserIDs(ctx context.Context, userID string) ([]string, error)
	BlockUser(ctx context.Context, userID, blockedUserId string) error
}

func (repo *userRepository) userCollection() *mongo.Collection {
	return repo.client.Database("messaging_service").Collection("users")
}

func NewMongoUserRepository(ctx context.Context, client *mongo.Client) (User, error) {
	repository := &userRepository{
		client,
	}

	// Ensure user collection has a unique index on username
	// This is fine to do on startup since the index will not be created if it already exists
	_, err := repository.userCollection().Indexes().CreateOne(ctx, mongo.IndexModel{
		Keys:    map[string]int{"username": 1},
		Options: options.Index().SetUnique(true),
	})
	if err != nil {
		zap.L().Error("Error creating unique index on user collection", zap.Error(err))

		return nil, err
	}

	return repository, nil
}

func (repo *userRepository) CreateNewUniqueUser(ctx context.Context, params models.NewUser) (*models.User, error) {
	var user user
	user.fromModel(params.User)

	result, err := repo.userCollection().InsertOne(ctx, &user)
	if err != nil {
		if mongo.IsDuplicateKeyError(err) {
			return nil, errors.Join(models.ErrUserAlreadyExists, err)
		}

		zap.L().Error("Error creating new user", zap.Error(err))

		return nil, err
	}

	user.ID = result.InsertedID.(primitive.ObjectID)

	return user.toModel(), nil
}

func (repo *userRepository) GetUserByUsername(ctx context.Context, username string) (*models.User, error) {
	user := &user{}

	err := repo.userCollection().FindOne(ctx, map[string]string{"username": username}).Decode(user)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, errors.Join(models.ErrUserDoesNotExist, err)
		}

		zap.L().Error("Error getting user by username", zap.Error(err))

		return nil, err
	}

	return user.toModel(), nil
}

func (repo *userRepository) GetUserByID(ctx context.Context, id string) (*models.User, error) {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		zap.L().Error("Error converting id to object id", zap.Error(err))

		return nil, err
	}

	user := &user{}

	err = repo.userCollection().FindOne(ctx, map[string]primitive.ObjectID{"_id": objectID}).Decode(user)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, errors.Join(models.ErrUserDoesNotExist, err)
		}

		zap.L().Error("Error getting user by id", zap.Error(err))

		return nil, err
	}

	return user.toModel(), nil
}

func (repo *userRepository) GetBlockedUserIDs(ctx context.Context, userID string) ([]string, error) {
	objectID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		zap.L().Error("Error converting id to object id", zap.Error(err))

		return nil, err
	}

	blockedUsers := &blocked_users{}
	err = repo.client.Database("messaging_service").Collection("blocked_users").FindOne(ctx, map[string]primitive.ObjectID{"user_id": objectID}).Decode(blockedUsers)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, nil
		}

		return nil, err
	}

	var blockedUserIDs []string
	for _, blockedUserID := range blockedUsers.BlockedUserIDs {
		blockedUserIDs = append(blockedUserIDs, blockedUserID.Hex())
	}

	return blockedUserIDs, nil
}

func (repo *userRepository) BlockUser(ctx context.Context, userID, blockedUserId string) error {
	objectID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		zap.L().Error("Error converting id to object id", zap.Error(err))

		return err
	}

	blockedUserID, err := primitive.ObjectIDFromHex(blockedUserId)
	if err != nil {
		zap.L().Error("Error converting id to object id", zap.Error(err))

		return err
	}

	_, err = repo.client.Database("messaging_service").Collection("blocked_users").UpdateOne(ctx, map[string]primitive.ObjectID{"user_id": objectID}, map[string]interface{}{
		"$addToSet": map[string]primitive.ObjectID{"blocked_users": blockedUserID},
	}, options.Update().SetUpsert(true))
	if err != nil {
		zap.L().Error("Error blocking user", zap.Error(err))

		return err
	}

	return nil
}
