package controllers

import (
	"messaging_service/internal/controllers/dto"
	"messaging_service/internal/services"

	"github.com/gofiber/fiber/v2"
)

type activityController struct {
	service services.Activity
}
type Activity interface {
	GetActivity(ctx *fiber.Ctx) error
}

func NewActivityController(service services.Activity) Activity {
	return &activityController{
		service: service,
	}
}

func (ctrl *activityController) GetActivity(ctx *fiber.Ctx) error {
	userID := ctx.Locals("userID").(string)

	activityModel, err := ctrl.service.GetActivity(ctx.Context(), userID)
	if err != nil {
		return ctx.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": "Internal server error",
		})
	}

	response := make([]dto.ActivityResponse, len(activityModel))
	for i, activity := range activityModel {
		response[i] = dto.ActivityFromModel(activity)
	}

	return ctx.Status(fiber.StatusOK).JSON(response)
}
