package models

import (
	"time"
)

type ActivityType string

type Activity struct {
	Type    ActivityType
	UserID  string
	Time    time.Time
	Details map[string]interface{}
}

const (
	ActivityLogin      ActivityType = "login"
	ActivityRegister   ActivityType = "register"
	ActivitySendMsg    ActivityType = "send_message"
	ActivityReceiveMsg ActivityType = "receive_messages"
	ActivityBlock      ActivityType = "block"
)
