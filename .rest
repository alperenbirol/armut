@artorias = eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2Nzg0MTI2MTksImlhdCI6MTY3ODMyNjIxOSwic3ViIjoiNjQwOTM5Y2JhYTY1Y2Y4MDc5MzQyOWU3In0.yEGKsrVi0Tgi-U0PRzt1myRBo11n5Zf9od8kX-OYJ1s
@another = eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2Nzg0MTI4NDMsImlhdCI6MTY3ODMyNjQ0Mywic3ViIjoiNjQwOTNhYWJhYTY1Y2Y4MDc5MzQyOWU5In0.7ke6tekYv-uHfUokH3-mmqQRoGPxZY18tcY1WhhyK7U
@localhost = http://127.0.0.1
@port = 8080
###

POST {{localhost}}:{{port}}/user/register HTTP/1.1
content-type: application/json

{
    "username": "another",
    "password": "sample"
}

###

POST {{localhost}}:{{port}}/user/login HTTP/1.1
content-type: application/json

{
    "username": "artorias",
    "password": "sample"
}

###

POST {{localhost}}:{{port}}/block HTTP/1.1
Authorization: {{another}}
content-type: application/json

{
    "username": "artorias"
}

###

GET {{localhost}}:{{port}}/activity HTTP/1.1
Authorization: {{artorias}}

###

GET {{localhost}}:{{port}}/message HTTP/1.1
Authorization: {{another}}

###

POST {{localhost}}:{{port}}/message HTTP/1.1
Authorization: {{artorias}}
content-type: application/json

{
    "receiver": "another",
    "content": "Hello!"
}