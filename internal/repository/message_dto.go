package repository

import (
	"messaging_service/internal/models"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type message struct {
	ID         primitive.ObjectID `bson:"_id,omitempty"`
	SenderID   primitive.ObjectID `bson:"sender_id"`
	ReceiverID primitive.ObjectID `bson:"receiver_id"`
	Content    string             `bson:"content"`
	Time       primitive.DateTime `bson:"time"`
}

func (message *message) toModel() *models.Message {
	return &models.Message{
		SenderID:   message.SenderID.Hex(),
		ReceiverID: message.ReceiverID.Hex(),
		Content:    message.Content,
		Time:       message.Time.Time(),
	}
}

func (message *message) fromModel(model models.Message) {
	message.SenderID, _ = primitive.ObjectIDFromHex(model.SenderID)
	message.ReceiverID, _ = primitive.ObjectIDFromHex(model.ReceiverID)
	message.Content = model.Content
	message.Time = primitive.NewDateTimeFromTime(model.Time)
}
