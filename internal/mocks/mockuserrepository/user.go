// Code generated by mockery v2.20.0. DO NOT EDIT.

package mockuserrepository

import (
	context "context"
	models "messaging_service/internal/models"

	mock "github.com/stretchr/testify/mock"
)

// User is an autogenerated mock type for the User type
type User struct {
	mock.Mock
}

type User_Expecter struct {
	mock *mock.Mock
}

func (_m *User) EXPECT() *User_Expecter {
	return &User_Expecter{mock: &_m.Mock}
}

// BlockUser provides a mock function with given fields: ctx, userID, blockedUserId
func (_m *User) BlockUser(ctx context.Context, userID string, blockedUserId string) error {
	ret := _m.Called(ctx, userID, blockedUserId)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, string) error); ok {
		r0 = rf(ctx, userID, blockedUserId)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// User_BlockUser_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'BlockUser'
type User_BlockUser_Call struct {
	*mock.Call
}

// BlockUser is a helper method to define mock.On call
//   - ctx context.Context
//   - userID string
//   - blockedUserId string
func (_e *User_Expecter) BlockUser(ctx interface{}, userID interface{}, blockedUserId interface{}) *User_BlockUser_Call {
	return &User_BlockUser_Call{Call: _e.mock.On("BlockUser", ctx, userID, blockedUserId)}
}

func (_c *User_BlockUser_Call) Run(run func(ctx context.Context, userID string, blockedUserId string)) *User_BlockUser_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(string), args[2].(string))
	})
	return _c
}

func (_c *User_BlockUser_Call) Return(_a0 error) *User_BlockUser_Call {
	_c.Call.Return(_a0)
	return _c
}

func (_c *User_BlockUser_Call) RunAndReturn(run func(context.Context, string, string) error) *User_BlockUser_Call {
	_c.Call.Return(run)
	return _c
}

// CreateNewUniqueUser provides a mock function with given fields: ctx, params
func (_m *User) CreateNewUniqueUser(ctx context.Context, params models.NewUser) (*models.User, error) {
	ret := _m.Called(ctx, params)

	var r0 *models.User
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, models.NewUser) (*models.User, error)); ok {
		return rf(ctx, params)
	}
	if rf, ok := ret.Get(0).(func(context.Context, models.NewUser) *models.User); ok {
		r0 = rf(ctx, params)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.User)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, models.NewUser) error); ok {
		r1 = rf(ctx, params)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// User_CreateNewUniqueUser_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'CreateNewUniqueUser'
type User_CreateNewUniqueUser_Call struct {
	*mock.Call
}

// CreateNewUniqueUser is a helper method to define mock.On call
//   - ctx context.Context
//   - params models.NewUser
func (_e *User_Expecter) CreateNewUniqueUser(ctx interface{}, params interface{}) *User_CreateNewUniqueUser_Call {
	return &User_CreateNewUniqueUser_Call{Call: _e.mock.On("CreateNewUniqueUser", ctx, params)}
}

func (_c *User_CreateNewUniqueUser_Call) Run(run func(ctx context.Context, params models.NewUser)) *User_CreateNewUniqueUser_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(models.NewUser))
	})
	return _c
}

func (_c *User_CreateNewUniqueUser_Call) Return(_a0 *models.User, _a1 error) *User_CreateNewUniqueUser_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *User_CreateNewUniqueUser_Call) RunAndReturn(run func(context.Context, models.NewUser) (*models.User, error)) *User_CreateNewUniqueUser_Call {
	_c.Call.Return(run)
	return _c
}

// GetBlockedUserIDs provides a mock function with given fields: ctx, userID
func (_m *User) GetBlockedUserIDs(ctx context.Context, userID string) ([]string, error) {
	ret := _m.Called(ctx, userID)

	var r0 []string
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, string) ([]string, error)); ok {
		return rf(ctx, userID)
	}
	if rf, ok := ret.Get(0).(func(context.Context, string) []string); ok {
		r0 = rf(ctx, userID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]string)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, userID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// User_GetBlockedUserIDs_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'GetBlockedUserIDs'
type User_GetBlockedUserIDs_Call struct {
	*mock.Call
}

// GetBlockedUserIDs is a helper method to define mock.On call
//   - ctx context.Context
//   - userID string
func (_e *User_Expecter) GetBlockedUserIDs(ctx interface{}, userID interface{}) *User_GetBlockedUserIDs_Call {
	return &User_GetBlockedUserIDs_Call{Call: _e.mock.On("GetBlockedUserIDs", ctx, userID)}
}

func (_c *User_GetBlockedUserIDs_Call) Run(run func(ctx context.Context, userID string)) *User_GetBlockedUserIDs_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(string))
	})
	return _c
}

func (_c *User_GetBlockedUserIDs_Call) Return(_a0 []string, _a1 error) *User_GetBlockedUserIDs_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *User_GetBlockedUserIDs_Call) RunAndReturn(run func(context.Context, string) ([]string, error)) *User_GetBlockedUserIDs_Call {
	_c.Call.Return(run)
	return _c
}

// GetUserByID provides a mock function with given fields: ctx, id
func (_m *User) GetUserByID(ctx context.Context, id string) (*models.User, error) {
	ret := _m.Called(ctx, id)

	var r0 *models.User
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, string) (*models.User, error)); ok {
		return rf(ctx, id)
	}
	if rf, ok := ret.Get(0).(func(context.Context, string) *models.User); ok {
		r0 = rf(ctx, id)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.User)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, id)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// User_GetUserByID_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'GetUserByID'
type User_GetUserByID_Call struct {
	*mock.Call
}

// GetUserByID is a helper method to define mock.On call
//   - ctx context.Context
//   - id string
func (_e *User_Expecter) GetUserByID(ctx interface{}, id interface{}) *User_GetUserByID_Call {
	return &User_GetUserByID_Call{Call: _e.mock.On("GetUserByID", ctx, id)}
}

func (_c *User_GetUserByID_Call) Run(run func(ctx context.Context, id string)) *User_GetUserByID_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(string))
	})
	return _c
}

func (_c *User_GetUserByID_Call) Return(_a0 *models.User, _a1 error) *User_GetUserByID_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *User_GetUserByID_Call) RunAndReturn(run func(context.Context, string) (*models.User, error)) *User_GetUserByID_Call {
	_c.Call.Return(run)
	return _c
}

// GetUserByUsername provides a mock function with given fields: ctx, username
func (_m *User) GetUserByUsername(ctx context.Context, username string) (*models.User, error) {
	ret := _m.Called(ctx, username)

	var r0 *models.User
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, string) (*models.User, error)); ok {
		return rf(ctx, username)
	}
	if rf, ok := ret.Get(0).(func(context.Context, string) *models.User); ok {
		r0 = rf(ctx, username)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.User)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, username)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// User_GetUserByUsername_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'GetUserByUsername'
type User_GetUserByUsername_Call struct {
	*mock.Call
}

// GetUserByUsername is a helper method to define mock.On call
//   - ctx context.Context
//   - username string
func (_e *User_Expecter) GetUserByUsername(ctx interface{}, username interface{}) *User_GetUserByUsername_Call {
	return &User_GetUserByUsername_Call{Call: _e.mock.On("GetUserByUsername", ctx, username)}
}

func (_c *User_GetUserByUsername_Call) Run(run func(ctx context.Context, username string)) *User_GetUserByUsername_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(string))
	})
	return _c
}

func (_c *User_GetUserByUsername_Call) Return(_a0 *models.User, _a1 error) *User_GetUserByUsername_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *User_GetUserByUsername_Call) RunAndReturn(run func(context.Context, string) (*models.User, error)) *User_GetUserByUsername_Call {
	_c.Call.Return(run)
	return _c
}

type mockConstructorTestingTNewUser interface {
	mock.TestingT
	Cleanup(func())
}

// NewUser creates a new instance of User. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewUser(t mockConstructorTestingTNewUser) *User {
	mock := &User{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
