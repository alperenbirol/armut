package controllers_test

import (
	"fmt"
	"io"
	"messaging_service/internal/controllers"
	"messaging_service/internal/mocks/mockactivityservice"
	"messaging_service/internal/models"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
)

type ActivityControllerTestSuite struct {
	suite.Suite

	mockActivityService *mockactivityservice.Activity
	router              *fiber.App
}

func TestActivityControllerTestSuite(t *testing.T) {
	suite.Run(t, new(ActivityControllerTestSuite))
}

func (suite *ActivityControllerTestSuite) SetupTest() {
	suite.mockActivityService = new(mockactivityservice.Activity)

	ctrl := controllers.NewActivityController(suite.mockActivityService)

	suite.router = fiber.New()

	suite.router.Use(func(c *fiber.Ctx) error {
		c.Locals("userID", "testUser")

		return c.Next()
	})

	suite.router.Get("/activity", ctrl.GetActivity)
}

func (suite *ActivityControllerTestSuite) TeardownTest() {
	suite.mockActivityService.AssertExpectations(suite.T())
}

func (suite *ActivityControllerTestSuite) TestGetActivity() {
	now := time.Now()
	register := now.Add(-time.Hour)
	login := now.Add(-time.Hour / 2)

	suite.mockActivityService.EXPECT().GetActivity(mock.Anything, "testUser").Return([]models.Activity{
		{
			Type:   models.ActivityRegister,
			UserID: "testUser",
			Time:   register,
		},
		{
			Type:    models.ActivityLogin,
			UserID:  "testUser",
			Time:    login,
			Details: map[string]interface{}{"success": true},
		},
	}, nil)

	req := httptest.NewRequest("GET", "/activity", nil)

	res, err := suite.router.Test(req)
	suite.NoError(err)

	suite.Equal(http.StatusOK, res.StatusCode)

	body, err := io.ReadAll(res.Body)
	suite.NoError(err)

	suite.Equal(fmt.Sprintf(
		`[{"event_type":"register","timestamp":%d},{"event_type":"login","success":true,"timestamp":%d}]`,
		register.Unix(), login.Unix()),
		string(body))
}

func (suite *ActivityControllerTestSuite) TestGetActivityServiceFailure() {
	suite.mockActivityService.EXPECT().GetActivity(mock.Anything, "testUser").Return(nil, fmt.Errorf("test error"))

	req := httptest.NewRequest("GET", "/activity", nil)

	res, err := suite.router.Test(req)
	suite.NoError(err)

	suite.Equal(http.StatusInternalServerError, res.StatusCode)
}
