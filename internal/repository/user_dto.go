package repository

import (
	"messaging_service/internal/models"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type user struct {
	ID       primitive.ObjectID `bson:"_id,omitempty"`
	Username string             `bson:"username"`
	Password string             `bson:"password"`
}

type blocked_users struct {
	UserID         primitive.ObjectID   `bson:"user_id"`
	BlockedUserIDs []primitive.ObjectID `bson:"blocked_users"`
}

func (user *user) toModel() *models.User {
	return &models.User{
		ID:       user.ID.Hex(),
		Username: user.Username,
		Password: user.Password,
	}
}

func (user *user) fromModel(model models.User) {
	user.ID, _ = primitive.ObjectIDFromHex(model.ID)
	user.Username = model.Username
	user.Password = model.Password
}
