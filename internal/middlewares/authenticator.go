package middlewares

import (
	"messaging_service/internal/services"

	"github.com/gofiber/fiber/v2"
)

type authenticator struct {
	userService services.User
}

type Authenticator interface {
	Authenticate(ctx *fiber.Ctx) error
}

func NewAuthenticator(userService services.User) Authenticator {
	return &authenticator{
		userService: userService,
	}
}

func (authenticator *authenticator) Authenticate(ctx *fiber.Ctx) error {
	authorizationHeader := ctx.Get("Authorization")
	if authorizationHeader == "" {
		return ctx.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"error": "Unauthorized",
		})
	}

	user, err := authenticator.userService.GetUserIDFromToken(ctx.Context(), authorizationHeader)
	if err != nil {
		return ctx.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"error": "Unauthorized",
		})
	}

	ctx.Locals("userID", user)

	return ctx.Next()
}
