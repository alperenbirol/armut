package services_test

import (
	"context"
	"fmt"
	"messaging_service/internal/mocks/mockactivityservice"
	"messaging_service/internal/mocks/mockmessagerepository"
	"messaging_service/internal/mocks/mockuserservice"
	"messaging_service/internal/models"
	"messaging_service/internal/services"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
)

type MessageServiceTestSuite struct {
	suite.Suite

	service services.Message

	mockRepo            *mockmessagerepository.Message
	mockUserService     *mockuserservice.User
	mockActivityService *mockactivityservice.Activity
}

func TestMessageServiceTestSuite(t *testing.T) {
	suite.Run(t, new(MessageServiceTestSuite))
}

func (s *MessageServiceTestSuite) SetupTest() {
	s.mockRepo = &mockmessagerepository.Message{}
	s.mockUserService = &mockuserservice.User{}
	s.mockActivityService = &mockactivityservice.Activity{}

	s.service = services.NewMessageService(s.mockRepo, s.mockUserService, s.mockActivityService)
}

func (s *MessageServiceTestSuite) TeardownTest() {
	s.mockRepo.AssertExpectations(s.T())
	s.mockUserService.AssertExpectations(s.T())
	s.mockActivityService.AssertExpectations(s.T())
}

func (s *MessageServiceTestSuite) TestGetUserMessages() {
	now := time.Now()
	s.mockRepo.EXPECT().GetUserMessages(mock.Anything, "123").Return([]models.Message{
		{
			SenderID:   "456",
			ReceiverID: "123",
			Content:    "test",
			Time:       now,
		},
	}, nil)
	s.mockUserService.EXPECT().GetUserByID(mock.Anything, "123").Return(&models.User{
		ID:       "123",
		Username: "receiver",
	}, nil).Once()
	s.mockUserService.EXPECT().GetUserByID(mock.Anything, "456").Return(&models.User{
		ID:       "456",
		Username: "sender",
	}, nil).Once()

	messages, err := s.service.GetUserMessages(context.Background(), "123")
	s.NoError(err)

	s.Equal([]models.MessageResponse{
		{
			SenderUsername:   "sender",
			ReceiverUsername: "receiver",
			Content:          "test",
			Time:             now,
		},
	},
		messages)
}

func (s *MessageServiceTestSuite) TestGetUserMessagesRepoFailure() {
	s.mockRepo.EXPECT().GetUserMessages(mock.Anything, "123").Return(nil, fmt.Errorf("test"))

	messages, err := s.service.GetUserMessages(context.Background(), "123")
	s.Error(err)

	s.Nil(messages)
}

func (s *MessageServiceTestSuite) TestGetUserMessagesNoMessages() {
	s.mockRepo.EXPECT().GetUserMessages(mock.Anything, "123").Return([]models.Message{}, nil)

	messages, err := s.service.GetUserMessages(context.Background(), "123")
	s.ErrorIs(err, models.ErrNoMessages)

	s.Nil(messages)
}

func (s *MessageServiceTestSuite) TestSendMessage() {
	s.mockUserService.EXPECT().GetUserByUsername(mock.Anything, "receiver").Return(&models.User{
		ID:       "456",
		Username: "receiver",
	}, nil)

	s.mockUserService.EXPECT().GetBlockedUserIDs(mock.Anything, "456").Return([]string{}, nil)

	s.mockRepo.EXPECT().SaveMessage(mock.Anything, mock.Anything).Return(nil)

	s.mockActivityService.EXPECT().LogActivity(mock.Anything, mock.Anything).Return(nil)

	err := s.service.SendMessage(context.Background(), models.MessageRequest{
		SenderID:         "123",
		ReceiverUsername: "receiver",
		Content:          "test",
	})

	s.NoError(err)
}

func (s *MessageServiceTestSuite) TestSendMessageGetReceiverUserFailure() {
	s.mockUserService.EXPECT().GetUserByUsername(mock.Anything, "receiver").Return(nil, fmt.Errorf("test"))

	err := s.service.SendMessage(context.Background(), models.MessageRequest{
		SenderID:         "123",
		ReceiverUsername: "receiver",
		Content:          "test",
	})

	s.Error(err)
}

func (s *MessageServiceTestSuite) TestSendMessageSameSenderReceiver() {
	s.mockUserService.EXPECT().GetUserByUsername(mock.Anything, "user").Return(&models.User{
		ID:       "123",
		Username: "user",
	}, nil)

	err := s.service.SendMessage(context.Background(), models.MessageRequest{
		SenderID:         "123",
		ReceiverUsername: "user",
		Content:          "test",
	})

	s.ErrorIs(err, models.ErrSameSenderReceiver)
}

func (s *MessageServiceTestSuite) TestSendMessageGetBlockedUsersFailure() {
	s.mockUserService.EXPECT().GetUserByUsername(mock.Anything, "receiver").Return(&models.User{
		ID:       "456",
		Username: "receiver",
	}, nil)

	s.mockUserService.EXPECT().GetBlockedUserIDs(mock.Anything, "456").Return([]string{}, fmt.Errorf("some error"))

	err := s.service.SendMessage(context.Background(), models.MessageRequest{
		SenderID:         "123",
		ReceiverUsername: "receiver",
		Content:          "test",
	})

	s.Error(err)
}

func (s *MessageServiceTestSuite) TestSendMessageBlockedUser() {
	s.mockUserService.EXPECT().GetUserByUsername(mock.Anything, "receiver").Return(&models.User{
		ID:       "456",
		Username: "receiver",
	}, nil)

	s.mockUserService.EXPECT().GetBlockedUserIDs(mock.Anything, "456").Return([]string{"123"}, nil)

	s.mockActivityService.EXPECT().LogActivity(mock.Anything, mock.Anything).Return(nil)

	err := s.service.SendMessage(context.Background(), models.MessageRequest{
		SenderID:         "123",
		ReceiverUsername: "receiver",
		Content:          "test",
	})

	s.ErrorIs(err, models.ErrSenderIsBlocked)
}
