package controllers_test

import (
	"fmt"
	"io"
	"messaging_service/internal/controllers"
	"messaging_service/internal/mocks/mockmessageservice"
	"messaging_service/internal/models"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
)

type MessageControllerSuite struct {
	suite.Suite

	mockService *mockmessageservice.Message

	router *fiber.App
}

func TestMessageControllerTestSuite(t *testing.T) {
	suite.Run(t, new(MessageControllerSuite))
}

func (s *MessageControllerSuite) SetupTest() {
	s.mockService = new(mockmessageservice.Message)

	s.router = fiber.New()

	messageController := controllers.NewMessageController(controllers.RequestParser{
		validator.New(),
	}, s.mockService)

	s.router.Use(func(c *fiber.Ctx) error {
		c.Locals("userID", "1")
		return c.Next()
	})

	s.router.Get("/message", messageController.GetMessages)
	s.router.Post("/message", messageController.SendMessage)
}

func (s *MessageControllerSuite) TeardownTest() {
	s.mockService.AssertExpectations(s.T())
}

func (s *MessageControllerSuite) TestGetMessages() {
	now := time.Now()

	s.mockService.EXPECT().GetUserMessages(mock.Anything, "1").Return([]models.MessageResponse{
		{
			SenderUsername:   "sender",
			ReceiverUsername: "receiver",
			Content:          "message content",
			Time:             now,
		},
	}, nil)

	resp, err := s.router.Test(httptest.NewRequest(http.MethodGet, "/message", nil))
	s.NoError(err)

	s.Equal(http.StatusOK, resp.StatusCode)

	body, err := io.ReadAll(resp.Body)
	s.NoError(err)

	s.JSONEq(`[{
		"sender": "sender",
		"content": "message content",
		"timestamp":`+fmt.Sprintf(`%d`, now.Unix())+`
	}]`, string(body))
}

func (s *MessageControllerSuite) TestGetMessagesNoMessages() {
	s.mockService.EXPECT().GetUserMessages(mock.Anything, "1").Return(nil, models.ErrNoMessages)

	resp, err := s.router.Test(httptest.NewRequest(http.MethodGet, "/message", nil))
	s.NoError(err)

	s.Equal(http.StatusNotFound, resp.StatusCode)
}

func (s *MessageControllerSuite) TestGetMessagesError() {
	s.mockService.EXPECT().GetUserMessages(mock.Anything, "1").Return(nil, fmt.Errorf("some error"))

	resp, err := s.router.Test(httptest.NewRequest(http.MethodGet, "/message", nil))
	s.NoError(err)

	s.Equal(http.StatusInternalServerError, resp.StatusCode)
}

func (s *MessageControllerSuite) TestSendMessage() {
	s.mockService.EXPECT().SendMessage(mock.Anything, models.MessageRequest{
		SenderID:         "1",
		ReceiverUsername: "receiver",
		Content:          "message content",
	}).Return(nil)

	req := httptest.NewRequest(http.MethodPost, "/message", strings.NewReader(`{
		"receiver": "receiver",
		"content": "message content"
	}`))
	req.Header.Set("Content-Type", "application/json")

	resp, err := s.router.Test(req)
	s.NoError(err)

	s.Equal(http.StatusCreated, resp.StatusCode)
}

func (s *MessageControllerSuite) TestSendMessageInvalidRequest() {
	req := httptest.NewRequest(http.MethodPost, "/message", strings.NewReader(`{
		"receiver": "receiver"
	}`))
	req.Header.Set("Content-Type", "application/json")

	resp, err := s.router.Test(req)
	s.NoError(err)

	s.Equal(http.StatusBadRequest, resp.StatusCode)
}

func (s *MessageControllerSuite) TestSendMessageError() {
	s.mockService.EXPECT().SendMessage(mock.Anything, models.MessageRequest{
		SenderID:         "1",
		ReceiverUsername: "receiver",
		Content:          "message content",
	}).Return(fmt.Errorf("some error"))

	req := httptest.NewRequest(http.MethodPost, "/message", strings.NewReader(`{
		"receiver": "receiver",
		"content": "message content"
	}`))
	req.Header.Set("Content-Type", "application/json")

	resp, err := s.router.Test(req)
	s.NoError(err)

	s.Equal(http.StatusInternalServerError, resp.StatusCode)
}

func (s *MessageControllerSuite) TestSendMessageReceiverNotFound() {
	s.mockService.EXPECT().SendMessage(mock.Anything, models.MessageRequest{
		SenderID:         "1",
		ReceiverUsername: "bad_receiver",
		Content:          "message content",
	}).Return(models.ErrUserDoesNotExist)

	req := httptest.NewRequest(http.MethodPost, "/message", strings.NewReader(`{
		"receiver": "bad_receiver",
		"content": "message content"
	}`))
	req.Header.Set("Content-Type", "application/json")

	resp, err := s.router.Test(req)
	s.NoError(err)

	s.Equal(http.StatusNotFound, resp.StatusCode)
}

func (s *MessageControllerSuite) TestSendMessageSenderIsBlocked() {
	s.mockService.EXPECT().SendMessage(mock.Anything, models.MessageRequest{
		SenderID:         "1",
		ReceiverUsername: "receiver",
		Content:          "message content",
	}).Return(models.ErrSenderIsBlocked)

	req := httptest.NewRequest(http.MethodPost, "/message", strings.NewReader(`{
		"receiver": "receiver",
		"content": "message content"
	}`))
	req.Header.Set("Content-Type", "application/json")

	resp, err := s.router.Test(req)
	s.NoError(err)

	s.Equal(http.StatusForbidden, resp.StatusCode)
}
