package controllers

import (
	"errors"
	"fmt"
	"messaging_service/internal/controllers/dto"
	"messaging_service/internal/models"
	"messaging_service/internal/services"

	"github.com/gofiber/fiber/v2"
)

type userController struct {
	RequestParser
	service services.User
}

type User interface {
	Register(ctx *fiber.Ctx) error
	Login(ctx *fiber.Ctx) error
	BlockUser(ctx *fiber.Ctx) error
}

func NewUserController(parser RequestParser, userService services.User) User {
	return &userController{
		RequestParser: parser,
		service:       userService,
	}
}

func (ctrl *userController) Register(ctx *fiber.Ctx) error {
	var request dto.Register

	if err := ctrl.parseAndValidate(ctx, &request); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	}

	token, err := ctrl.service.CreateNewUser(ctx.Context(), request.ToModel())
	if err != nil {
		if errors.Is(err, models.ErrUserAlreadyExists) {
			return fiber.NewError(fiber.StatusConflict, fmt.Sprintf("username %s is already taken", request.Username))
		}

		return fiber.NewError(fiber.StatusInternalServerError)
	}

	return ctx.Status(fiber.StatusCreated).JSON(dto.TokenResponse{
		AccessToken: token,
	})
}

func (ctrl *userController) Login(ctx *fiber.Ctx) error {
	var request dto.Login

	if err := ctrl.parseAndValidate(ctx, &request); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	}

	token, err := ctrl.service.AuthenticateUser(ctx.Context(), request.ToModel())
	if err != nil {
		if errors.Is(err, models.ErrUserDoesNotExist) {
			return fiber.NewError(fiber.StatusNotFound, fmt.Sprintf("user %s does not exist", request.Username))
		}

		if errors.Is(err, models.ErrWrongPassword) {
			return fiber.NewError(fiber.StatusUnauthorized, fmt.Sprintf("wrong password for user %s", request.Username))
		}

		return fiber.NewError(fiber.StatusInternalServerError)
	}

	return ctx.Status(fiber.StatusOK).JSON(dto.TokenResponse{
		AccessToken: token,
	})
}

func (ctrl *userController) BlockUser(ctx *fiber.Ctx) error {
	var request dto.BlockUserRequest

	if err := ctrl.parseAndValidate(ctx, &request); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	}

	userID := ctx.Locals("userID").(string)

	if err := ctrl.service.BlockUser(ctx.Context(), userID, request.Username); err != nil {
		return fiber.NewError(fiber.StatusInternalServerError)
	}

	return ctx.Status(fiber.StatusOK).JSON("user blocked")
}
