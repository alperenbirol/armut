package models

import (
	"errors"

	"golang.org/x/crypto/bcrypt"
)

type User struct {
	ID       string
	Username string
	Password string
}

type NewUser struct {
	User
}

func (user *NewUser) HashPassword() error {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	user.Password = string(hashedPassword)

	return nil
}

type SignInUser struct {
	User
}

func (user *SignInUser) ComparePassword(hashedPassword string) error {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(user.Password))
	if errors.Is(err, bcrypt.ErrMismatchedHashAndPassword) {
		return ErrWrongPassword
	}

	return err
}

var ErrUserAlreadyExists = errors.New("user with that username already exists")
var ErrUserDoesNotExist = errors.New("user does not exist")
var ErrWrongPassword = errors.New("wrong password")
