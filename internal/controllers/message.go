package controllers

import (
	"errors"
	"messaging_service/internal/controllers/dto"
	"messaging_service/internal/models"
	"messaging_service/internal/services"

	"github.com/gofiber/fiber/v2"
)

type messageController struct {
	RequestParser
	service services.Message
}

type Message interface {
	GetMessages(ctx *fiber.Ctx) error
	SendMessage(ctx *fiber.Ctx) error
}

func NewMessageController(parser RequestParser, service services.Message) Message {
	return &messageController{
		RequestParser: parser,
		service:       service,
	}
}

func (ctrl *messageController) GetMessages(ctx *fiber.Ctx) error {
	userID := ctx.Locals("userID").(string)

	messages, err := ctrl.service.GetUserMessages(ctx.Context(), userID)
	if err != nil {
		if errors.Is(err, models.ErrNoMessages) {
			return ctx.Status(fiber.StatusNotFound).JSON("no messages found")
		}

		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}

	var response = make([]dto.MessageResponse, len(messages))
	for i, message := range messages {
		response[i].FromModel(message)
	}

	return ctx.Status(fiber.StatusOK).JSON(response)
}

func (ctrl *messageController) SendMessage(ctx *fiber.Ctx) error {
	var params dto.MessageRequest

	if err := ctrl.parseAndValidate(ctx, &params); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	}

	userID := ctx.Locals("userID").(string)

	if err := ctrl.service.SendMessage(ctx.Context(), params.ToModel(userID)); err != nil {
		if errors.Is(err, models.ErrUserDoesNotExist) {
			return ctx.Status(fiber.StatusNotFound).JSON("user not found")
		}

		if errors.Is(err, models.ErrSenderIsBlocked) {
			return ctx.Status(fiber.StatusForbidden).JSON(err.Error())
		}

		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}

	return ctx.Status(fiber.StatusCreated).JSON("message sent")
}
