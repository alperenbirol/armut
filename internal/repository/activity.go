package repository

import (
	"context"
	"messaging_service/internal/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"
)

type activityRepository struct {
	client *mongo.Client
}

//go:generate mockery --name Activity --output ../mocks/mockactivityrepository --outpkg mockactivityrepository --case underscore --with-expecter
type Activity interface {
	LogActivity(ctx context.Context, params models.Activity) error
	GetActivity(ctx context.Context, userID string) ([]models.Activity, error)
}

func NewMongoActivityRepository(client *mongo.Client) Activity {
	return &activityRepository{
		client: client,
	}
}

func (repo *activityRepository) activityCollection() *mongo.Collection {
	return repo.client.Database("messaging_service").Collection("activity")
}

func (repo *activityRepository) LogActivity(ctx context.Context, params models.Activity) error {
	var activity activity
	activity.fromModel(params)

	_, err := repo.activityCollection().InsertOne(ctx, &activity)
	if err != nil {
		zap.L().Error("Error logging activity", zap.Error(err))

		return err
	}

	return nil
}

func (repo *activityRepository) GetActivity(ctx context.Context, userID string) ([]models.Activity, error) {
	var activities []activity

	userIDObjectID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		zap.L().Error("invalid userID", zap.Error(err))

		return nil, err
	}
	cursor, err := repo.activityCollection().Find(ctx,
		bson.D{{Key: "user_id", Value: userIDObjectID}},
		options.Find().SetSort(bson.D{{Key: "timestamp", Value: 1}}))
	if err != nil {
		zap.L().Error("Error getting activity", zap.Error(err))

		return nil, err
	}

	err = cursor.All(ctx, &activities)
	if err != nil {
		zap.L().Error("Error getting activity", zap.Error(err))

		return nil, err
	}

	var result []models.Activity = make([]models.Activity, len(activities))
	for index, activity := range activities {
		result[index] = *activity.toModel()
	}

	return result, nil
}
