package main

import (
	"context"
	"messaging_service/internal/app"
	"messaging_service/internal/controllers"
	"messaging_service/internal/middlewares"
	"messaging_service/internal/repository"
	"messaging_service/internal/services"
	"messaging_service/internal/token"
	"os"
	"strconv"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
	_ "github.com/joho/godotenv/autoload"
)

type config struct {
	DatabaseDSN string
	JWTSecret   string
	Port        uint
}

func getConfig() config {
	databaseDSN := os.Getenv("MONGO_DSN")
	if databaseDSN == "" {
		zap.L().Fatal("MONGO_DSN environment variable not set")
	}

	jwtSecret := os.Getenv("JWT_SECRET")
	if jwtSecret == "" {
		jwtSecret = "supersecret" + uuid.New().String()
	}
	port, err := strconv.ParseUint(os.Getenv("PORT"), 10, 32)
	if err != nil {
		port = 8080
	}

	return config{
		DatabaseDSN: databaseDSN,
		JWTSecret:   jwtSecret,
		Port:        uint(port),
	}
}

func main() {
	logger, err := zap.NewDevelopment()
	if err != nil {
		logger = zap.New(zapcore.NewCore(
			zapcore.NewConsoleEncoder(zap.NewDevelopmentEncoderConfig()),
			zapcore.Lock(os.Stdout),
			zapcore.DebugLevel,
		))
	}
	zap.ReplaceGlobals(logger)

	cfg := getConfig()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	clientOptions := options.Client().
		ApplyURI(cfg.DatabaseDSN).
		SetServerAPIOptions(
			options.ServerAPI(options.ServerAPIVersion1),
		)

	mongoClient, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		zap.L().Fatal("Error on connecting to MongoDB", zap.Error(err))
	}

	userRepository, err := repository.NewMongoUserRepository(ctx, mongoClient)
	if err != nil {
		zap.L().Fatal("Error on user repository", zap.Error(err))
	}
	activityRepository := repository.NewMongoActivityRepository(mongoClient)
	messageRepository := repository.NewMongoMessageRepository(mongoClient)

	jwtTokenHandler := token.NewJWTHandler(cfg.JWTSecret)

	activityService := services.NewActivityService(activityRepository)
	userService := services.NewUserService(userRepository, activityService, jwtTokenHandler)
	messageService := services.NewMessageService(
		messageRepository,
		userService,
		activityService,
	)

	authenticator := middlewares.NewAuthenticator(userService)

	requestParser := controllers.RequestParser{
		Validator: validator.New(),
	}

	userController := controllers.NewUserController(
		requestParser,
		userService,
	)
	activityController := controllers.NewActivityController(activityService)
	messageController := controllers.NewMessageController(
		requestParser,
		messageService,
	)

	app := app.App{
		Authenticator:      authenticator,
		UserController:     userController,
		ActivityController: activityController,
		MessageController:  messageController,
	}

	if err := app.Run(cfg.Port); err != nil {
		zap.L().Fatal("Error on running app", zap.Error(err))
	}
}
