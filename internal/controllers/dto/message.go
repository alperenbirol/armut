package dto

import "messaging_service/internal/models"

type MessageRequest struct {
	ReceiverUsername string `json:"receiver" validate:"required"`
	Content          string `json:"content" validate:"required"`
}

func (m *MessageRequest) ToModel(senderID string) models.MessageRequest {
	return models.MessageRequest{
		SenderID:         senderID,
		ReceiverUsername: m.ReceiverUsername,
		Content:          m.Content,
	}
}

type MessageResponse struct {
	SenderUsername string `json:"sender"`
	Content        string `json:"content"`
	Timestamp      int64  `json:"timestamp"`
}

func (m *MessageResponse) FromModel(model models.MessageResponse) {
	m.SenderUsername = model.SenderUsername
	m.Content = model.Content
	m.Timestamp = model.Time.Unix()
}
