package services_test

import (
	"context"
	"fmt"
	"messaging_service/internal/mocks/mockactivityrepository"
	"messaging_service/internal/models"
	"messaging_service/internal/services"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
)

type ActivityServiceTestSuite struct {
	suite.Suite

	service services.Activity

	mockRepo *mockactivityrepository.Activity
}

func TestActivityServiceTestSuite(t *testing.T) {
	suite.Run(t, new(ActivityServiceTestSuite))
}

func (suite *ActivityServiceTestSuite) SetupTest() {
	suite.mockRepo = &mockactivityrepository.Activity{}
	suite.service = services.NewActivityService(suite.mockRepo)
}

func (suite *ActivityServiceTestSuite) TeardownTest() {
	suite.mockRepo.AssertExpectations(suite.T())
}

func (suite *ActivityServiceTestSuite) TestLogActivity() {
	params := models.Activity{
		Type:   models.ActivityLogin,
		UserID: "123",
		Time:   time.Now(),
		Details: map[string]interface{}{
			"success": true,
		},
	}
	suite.mockRepo.EXPECT().LogActivity(context.Background(), params).Return(nil)

	err := suite.service.LogActivity(context.Background(), params)
	suite.NoError(err)
}

func (suite *ActivityServiceTestSuite) TestLogActivityRepositoryFailure() {
	params := models.Activity{
		Type:   models.ActivityLogin,
		UserID: "123",
		Time:   time.Now(),
		Details: map[string]interface{}{
			"success": true,
		},
	}
	suite.mockRepo.EXPECT().LogActivity(context.Background(), params).Return(fmt.Errorf("some error"))

	err := suite.service.LogActivity(context.Background(), params)
	suite.Error(err)
}

func (suite *ActivityServiceTestSuite) TestGetActivity() {
	params := "123"
	expected := []models.Activity{
		{
			Type:   models.ActivityLogin,
			UserID: "123",
			Time:   time.Now(),
			Details: map[string]interface{}{
				"success": true,
			},
		},
	}
	suite.mockRepo.EXPECT().GetActivity(context.Background(), params).Return(expected, nil)

	activities, err := suite.service.GetActivity(context.Background(), params)
	suite.NoError(err)
	suite.Equal(expected, activities)
}

func (suite *ActivityServiceTestSuite) TestGetActivityRepositoryFailure() {
	params := "123"
	suite.mockRepo.EXPECT().GetActivity(context.Background(), params).Return(nil, fmt.Errorf("some error"))

	activities, err := suite.service.GetActivity(context.Background(), params)
	suite.Error(err)
	suite.Nil(activities)
}
