package repository

import (
	"messaging_service/internal/models"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type activityDetails map[string]interface{}

type activity struct {
	ID      primitive.ObjectID `bson:"_id,omitempty"`
	Type    string             `bson:"type"`
	UserID  primitive.ObjectID `bson:"user_id"`
	Time    primitive.DateTime `bson:"time"`
	Details activityDetails    `bson:"details,inline"`
}

func (activity *activity) toModel() *models.Activity {
	return &models.Activity{
		Type:    models.ActivityType(activity.Type),
		UserID:  activity.UserID.Hex(),
		Time:    activity.Time.Time(),
		Details: activity.Details,
	}
}

func (activity *activity) fromModel(model models.Activity) {
	activity.Type = string(model.Type)
	activity.UserID, _ = primitive.ObjectIDFromHex(model.UserID)
	activity.Time = primitive.NewDateTimeFromTime(model.Time)
	activity.Details = model.Details
}
