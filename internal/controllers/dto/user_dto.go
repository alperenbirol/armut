package dto

import "messaging_service/internal/models"

type Login struct {
	Username string `json:"username" validate:"required"`
	Password string `json:"password" validate:"required"`
}

type Register struct {
	Username string `json:"username" validate:"required"`
	Password string `json:"password" validate:"required,max=72"`
}

func (dto *Login) ToModel() models.SignInUser {
	return models.SignInUser{
		User: models.User{
			Username: dto.Username,
			Password: dto.Password,
		},
	}
}

func (dto *Register) ToModel() models.NewUser {
	return models.NewUser{
		User: models.User{
			Username: dto.Username,
			Password: dto.Password,
		},
	}
}

type TokenResponse struct {
	AccessToken string `json:"access_token"`
}

type BlockUserRequest struct {
	Username string `json:"username" validate:"required"`
}
